<?php

namespace Drupal\cl_selector_field\Plugin\Field\FieldWidget;

use Drupal\cl_components\Exception\ComponentNotFoundException;
use Drupal\cl_components\Plugin\Component;
use Drupal\cl_editorial\NoThemeComponentManager;
use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines the 'cl_selector_field_style_selector' field widget.
 *
 * @FieldWidget(
 *   id = "cl_selector_field_style_selector",
 *   label = @Translation("Style Selector"),
 *   field_types = {"cl_selector_field_style_selector"},
 * )
 */
class StyleSelectorWidget extends WidgetBase {

  /**
   * The component manager.
   *
   * @var \Drupal\cl_editorial\NoThemeComponentManager
   */
  private NoThemeComponentManager $componentManager;

  /**
   * The path where Drupal is installed.
   *
   * @var string
   */
  private string $appRoot;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, NoThemeComponentManager $component_manager, string $app_root) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->componentManager = $component_manager;
    $this->appRoot = $app_root;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $component_manager = $container->get(NoThemeComponentManager::class);
    assert($component_manager instanceof NoThemeComponentManager);
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $component_manager,
      $container->getParameter('app.root')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $item = $items[$delta] ?? NULL;
    $settings = $items->getSettings();
    $collect_variant = (bool) ($settings['features']['variant'] ?? NULL);
    $all_parents = array_merge(
      $element['#field_parents'],
      [$items->getName(), $delta, 'component']
    );
    $all_user_input = $form_state->getUserInput();
    $user_input = NestedArray::getValue($all_user_input, $all_parents) ?? [];
    $selected_component = NestedArray::getValue($user_input, ['machine_name']);
    $selected_component = !is_null($selected_component)
      ? $selected_component
      : ($item->component ?? NULL);
    $component = NULL;
    if ($selected_component) {
      try {
        $component = $this->componentManager->findWithoutThemeFilter($selected_component);
      }
      catch (ComponentNotFoundException $e) {
        $selected_component = NULL;
      }
    }
    $filter_props = ['allowed', 'forbidden', 'statuses', 'types'];
    $filters = array_intersect_key($settings['filters'], array_flip($filter_props));
    $component_element = [
      ...$element,
      '#type' => 'cl_component_selector',
      '#default_value' => ['machine_name' => $selected_component],
      '#filters' => $filters,
    ];
    $field_name = $items->getFieldDefinition()->getName();
    $wrapper_id = Html::getId(sprintf('%s-%d-form-element-wrapper', $field_name, $delta));
    if ($collect_variant) {
      $component_element['#ajax'] = [
        'wrapper' => $wrapper_id,
        'callback' => [static::class, 'ajaxDataHandler'],
      ];
    }
    $element = ['component' => $component_element];
    if ($collect_variant && $component instanceof Component && !empty($component->getVariants())) {
      $selected_variant = NestedArray::getValue($user_input, ['variant']);
      $selected_variant = !is_null($selected_variant)
        ? $selected_variant
        : ($item->variant ?? NULL);
      $variants = $component->getVariants();
      $options = array_map(
        static fn(string $variant) => ucwords(preg_replace('/[^a-zA-Z0-9]/', ' ', $variant)),
        array_combine($variants, $variants)
      );
      $element['component']['variant'] = [
        '#type' => 'select',
        '#title' => $this->t('Variant'),
        '#description' => $this->t('The component variant to use for this selection.'),
        '#options' => ['' => $this->t('- Default variant -'), ...$options],
        '#default_value' => empty($options[$selected_variant]) ? '' : $selected_variant,
        '#weight' => 5,
      ];
    }
    else {
      $element['component']['variant'] = ['#type' => 'hidden', '#value' => ''];
    }
    return [
      '#type' => 'container',
      '#attributes' => ['id' => $wrapper_id],
      ...$element,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as $delta => $value) {
      $machine_name = $value['component']['machine_name'] ?? '';
      if ($machine_name === '') {
        $values[$delta]['component'] = NULL;
        $values[$delta]['variant'] = NULL;
        continue;
      }
      $variant = $value['component']['variant'] ?? '';
      $variant = empty($variant) ? NULL : $variant;
      $values[$delta]['component'] = $machine_name;
      $values[$delta]['variant'] = $variant;
    }
    return $values;
  }

  /**
   * Handles the AJAX data.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form interface.
   *
   * @return array|mixed|null
   *   The results.
   */
  public static function ajaxDataHandler(array &$form, FormStateInterface $form_state) {
    // Pop the 3 last elements so we render the whole thing.
    $triggering_element = $form_state->getTriggeringElement();
    $parents = $triggering_element['#array_parents'] ?? [];
    return NestedArray::getValue($form, array_slice($parents, 0, -3));
  }

}
