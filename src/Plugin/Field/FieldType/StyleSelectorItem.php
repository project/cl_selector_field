<?php

namespace Drupal\cl_selector_field\Plugin\Field\FieldType;

use Drupal\cl_components\Component\ComponentMetadata;
use Drupal\cl_components\Plugin\Component;
use Drupal\cl_editorial\Form\ComponentFiltersFormTrait;
use Drupal\cl_editorial\NoThemeComponentManager;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Defines the 'cl_selector_field_style_selector' field type.
 *
 * @FieldType(
 *   id = "cl_selector_field_style_selector",
 *   label = @Translation("Style Selector"),
 *   category = @Translation("General"),
 *   default_widget = "cl_selector_field_style_selector",
 *   default_formatter = "cl_selector_field_style_selector_table"
 * )
 */
class StyleSelectorItem extends FieldItemBase {

  use ComponentFiltersFormTrait;

  const VARIANT = 'variant';

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    $default_settings = [
      'features' => [
        static::VARIANT => static::VARIANT,
      ],
      'filters' => [
        'forbidden' => [],
        'allowed' => [],
        'statuses' => [
          ComponentMetadata::COMPONENT_STATUS_READY,
          ComponentMetadata::COMPONENT_STATUS_BETA,
          ComponentMetadata::COMPONENT_STATUS_WIP,
          ComponentMetadata::COMPONENT_STATUS_DEPRECATED,
        ],
        'types' => [
          ComponentMetadata::COMPONENT_TYPE_ORGANISM,
          ComponentMetadata::COMPONENT_TYPE_MOLECULE,
          ComponentMetadata::COMPONENT_TYPE_ATOM,
        ],
      ],
    ];
    return $default_settings + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $settings = $this->getSettings();
    $message = $this->t('Set a combination of filters to control the list of components that will become blocks.');
    $parents = ['settings'];
    $form_settings = $settings['filters'];
    $component_manager = \Drupal::service(NoThemeComponentManager::class);
    static::buildSettingsForm($form, $form_state, $component_manager, $form_settings, $parents, $message);

    $form['features'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Supported features'),
      '#description' => $this->t('The component is mandatory, but the variant support is optional for each field instance.'),
      '#default_value' => $settings['features'],
      '#options' => [
        static::VARIANT => $this->t('Variant'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public static function fieldSettingsToConfigData(array $settings) {
    // Allowed anf forbidden are nested in the form due to AJAX reasons. We undo
    // that here for config storage.
    $clean = static fn (array $item) => array_values(array_filter($item));
    $new_settings = [
      'features' => $settings['features'],
      'filters' => [
        'statuses' => $clean($settings['filters']['statuses'] ?? []),
        'types' => $clean($settings['filters']['types'] ?? []),
        'allowed' => $clean($settings['filters']['refine']['allowed'] ?? []),
        'forbidden' => $clean($settings['filters']['statuses']['forbidden'] ?? []),
      ],
    ];
    return parent::fieldSettingsToConfigData($new_settings);
  }

  /**
   * Returns all the allowed values for 'component' sub-field grouped by letter.
   *
   * @param array $allowed
   *   The allowed components.
   * @param array $forbidden
   *   The forbidden components.
   *
   * @return array
   *   The list of allowed values.
   */
  private function allAllowedComponents(array $allowed, array $forbidden, array $types, array $statuses): array {
    $component_manager = \Drupal::service(NoThemeComponentManager::class);
    assert($component_manager instanceof NoThemeComponentManager);
    $components = $component_manager->getFilteredComponentTypes(
        $allowed,
        $forbidden,
        [
          ComponentMetadata::COMPONENT_TYPE_ORGANISM,
          ComponentMetadata::COMPONENT_TYPE_MOLECULE,
          ComponentMetadata::COMPONENT_TYPE_ATOM,
        ],
        [
          ComponentMetadata::COMPONENT_STATUS_READY,
          ComponentMetadata::COMPONENT_STATUS_BETA,
          ComponentMetadata::COMPONENT_STATUS_WIP,
          ComponentMetadata::COMPONENT_STATUS_DEPRECATED,
        ]
      );
    return array_reduce(
      $components,
      static fn(array $carry, Component $component) => [
        ...$carry,
        $component->getId() => $component,
      ],
      []
    );
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    return $this->component === NULL
      && $this->variant === NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['component'] = DataDefinition::create('string')
      ->setLabel(t('Component'));
    $properties['variant'] = DataDefinition::create('string')
      ->setLabel(t('Variant'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();
    $clean = static fn(array $a) => array_values(array_filter($a));
    $definition = $this->getFieldDefinition();
    $filters = $definition->getSetting('filters');
    $allowed = $clean($filters['allowed'] ?? []);
    $forbidden = $clean($filters['forbidden'] ?? []);
    $types = $clean($filters['types'] ?? []);
    $statuses = $clean($filters['statuses'] ?? []);
    $values = $this->allAllowedComponents($allowed, $forbidden, $types, $statuses);
    $options['component'] = [];
    if (!empty($this->values)) {
      $options['component']['AllowedValues'] = array_keys($values);
      $options['component']['AllowedValues'][] = NULL;
    }

    $constraint_manager = $this->getTypedDataManager()
      ->getValidationConstraintManager();
    $constraints[] = $constraint_manager->create('ComplexData', $options);
    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'component' => [
          'type' => 'varchar',
          'length' => 255,
        ],
        'variant' => [
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return [
      'component' => 'my-component',
      'variant' => 'my-variant',
    ];
  }

}
